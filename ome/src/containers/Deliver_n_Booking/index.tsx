import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import MoreIcon from "../../icons/more_icon";

function DeliverAndBookingContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_deliver_n_booking}>
      <div className={styles.title_deliver_n_booking}>
        <p>Giao hàng và đặt xe</p>{" "}
        <div className={styles.suggest_text}>
          <div>Gợi ý</div>
        </div>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.all_game_container_list}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        <div
          className={styles.box_deliver_n_booking}
          style={{
            width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
          }}
        >
          <div
            className="d-flex align-items-start justify-content-start  mb-3 "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={classnames(styles.card_deliver_n_booking)}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/deliver_n_booking/uber_icon.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Uber</p>
              <p>
                Uber là một công ty đa quốc gia của Mỹ cung cấp các dịch vụ giao
                thông vận tải thông qua một ứng dụng công nghệ. 
              </p>
              <p>Tiện ích | + 100k người dùng</p>
            </div>
          </div>

          <div
            className="d-flex align-items-start justify-content-start  mb-3  "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={styles.card_deliver_n_booking}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/deliver_n_booking/grab_icon.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Grab </p>
              <p>
                Grab là ứng dụng đặt xe tiện lợi được yêu chuộng nhất Đông Nam Á
                với mục đích mang đến cho người dùng các dịch vụ vận chuyển
                nhanh chóng, bằng cách kết nối hơn 10 triệu hành
              </p>
              <p>Tiện ích | + 100k người dùng</p>
            </div>
          </div>

          <div
            className="d-flex align-items-start justify-content-start  mb-3  "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={styles.card_deliver_n_booking}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/deliver_n_booking/be_icon.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Be</p>
              <p>
                Be là ứng dụng gọi xe công nghệ được phát triển bởi Công ty cổ
                phần Be GROUP, startup công nghệ Việt Nam, 
              </p>
              <p>Tiện ích | + 100k người dùng</p>
            </div>
          </div>
        </div>

        <div
          className={styles.box_deliver_n_booking}
          style={{
            width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
          }}
        >
          <div
            className="d-flex align-items-start justify-content-start  mb-3 "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={classnames(styles.card_deliver_n_booking)}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/deliver_n_booking/go_viet_icon.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Uber</p>
              <p>
                Uber là một công ty đa quốc gia của Mỹ cung cấp các dịch vụ giao
                thông vận tải thông qua một ứng dụng công nghệ. 
              </p>
              <p>Tiện ích | + 100k người dùng </p>
            </div>
          </div>

          <div
            className="d-flex align-items-start justify-content-start  mb-3  "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={styles.card_deliver_n_booking}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/deliver_n_booking/go_jek_icon.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Grab</p>
              <p>
                Grab là ứng dụng đặt xe tiện lợi được yêu chuộng nhất Đông Nam Á
                với mục đích mang đến cho người dùng các dịch vụ vận chuyển
                nhanh chóng, bằng cách kết nối hơn 10 triệu hành
              </p>
              <p>Tiện ích | + 100k người dùng</p>
            </div>
          </div>

          <div
            className="d-flex align-items-start justify-content-start  mb-3  "
            style={{
              width: widthForListItem - 40 < 316 ? 316 : widthForListItem - 40,
            }}
          >
            <div className={styles.card_deliver_n_booking}>
              <Card
                style={{ width: "48px", height: "48px", borderRadius: "4px" }}
              >
                <CardMedia
                  component="img"
                  width="48"
                  height="48"
                  image="/images/deliver_n_booking/xanh_sm_icon.png"
                />
              </Card>
            </div>
            <div className={classnames(styles.content_item_list, "px-2")}>
              <p>Be</p>
              <p>
                Be là ứng dụng gọi xe công nghệ được phát triển bởi Công ty cổ
                phần Be GROUP, startup công nghệ Việt Nam, 
              </p>
              <p>Tiện ích | + 100k người dùng</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DeliverAndBookingContent;
