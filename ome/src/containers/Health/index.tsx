import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import MoreIcon from "../../icons/more_icon";

function HealthContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_health}>
      <div className={styles.title_health}>
        <p>Sức khỏe</p>{" "}
        <div className={styles.suggest_text}>
          <div>Gợi ý</div>
        </div>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_health}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        <div className="d-flex align-items-start justify-content-start  mb-3 ">
          <div className={classnames(styles.card_health)}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/health/pulse_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>We Do Pulse</p>
            <p>
              PULSE by Prudential là gì? Ứng dụng chăm sóc và quản lý sức khỏe
              toàn diện, từ thể chất tới tinh thần, giúp bạn có một cuộc sống
              cân bằng, tích cực và hiệu .
            </p>
            <p>Sức khỏe | + 100k người dùng</p>
          </div>
        </div>
        <div className="d-flex align-items-start justify-content-start  mb-3  ">
          <div className={styles.card_health}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/health/google_fit_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>Google Fit</p>
            <p>
              Hãy để Google Fit phiên bản mới giúp bạn sống lành mạnh và tích
              cực hơn! Bạn khó mà biết được mình cần thực hiện loại hoạt động
              nào hoặc mức độ tập luyện ra ...
            </p>
            <p>Sức khỏe | + 100k người dùng</p>
          </div>
        </div>
        <div className="d-flex align-items-start justify-content-start  mb-3  ">
          <div className={styles.card_health}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/health/bmi_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>BMI Calculator</p>
            <p>
              BMI Calculator helps you to calculate your Body Mass Index (BMI)
              based on the relevant information on body weight, height, age and
              sex.
            </p>
            <p>Sức khỏe | + 100k người dùng</p>
          </div>
        </div>
      </div>

      <div className={styles.more_text}>
        <div className={styles.icon_button}>
          <MoreIcon></MoreIcon>
          <span>Xem thêm</span>
        </div>
      </div>
    </div>
  );
}

export default HealthContent;
