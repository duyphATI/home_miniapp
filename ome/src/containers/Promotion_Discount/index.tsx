import React, { useMemo } from "react";
import Slider from "react-slick";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";

function PromotionDiscountContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForSlider = useMemo(() => {
    const tempWidth = width - 140;
    return tempWidth;
  }, [width]);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    initialSlide: 0,

    // turn off show arrow --> will break device screen
    arrows: false,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <div className={styles.container_promotion_discount}>
      <div className={styles.title_promotion_discount}>
        <p>Khuyến mại - Giảm giá sốc</p>{" "}
        <div className={styles.suggest_text}>
          <div>Gợi ý</div>
        </div>
      </div>

      <div className={styles.slider_container}>
        <Slider {...settings}>
          <div className={styles.card_promotion_discount}>
            <Card>
              <CardMedia
                height="180"
                width={widthForSlider}
                component="img"
                image="/images/promotion_discount/banner_promotion_discount.png"
              />
            </Card>
          </div>

          <div className={styles.card_promotion_discount}>
            <Card>
              <CardMedia
                width={widthForSlider}
                height="180"
                component="img"
                image="/images/promotion_discount/banner_hot_deal_1.png"
              />
            </Card>
          </div>

          <div className={styles.card_promotion_discount}>
            <Card>
              <CardMedia
                width={widthForSlider}
                height="180"
                component="img"
                image="/images/promotion_discount/banner_hot_deal_2.png"
              />
            </Card>
          </div>

          <div className={styles.card_promotion_discount}>
            <Card>
              <CardMedia
                width={widthForSlider}
                height="180"
                component="img"
                image="/images/promotion_discount/banner_promotion_1.png"
              />
            </Card>
          </div>

          <div className={styles.card_promotion_discount}>
            <Card>
              <CardMedia
                width={widthForSlider}
                height="180"
                component="img"
                image="/images/promotion_discount/banner_promotion_2.png"
              />
            </Card>
          </div>
        </Slider>
      </div>
    </div>
  );
}

export default PromotionDiscountContent;
