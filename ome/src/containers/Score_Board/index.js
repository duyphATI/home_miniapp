import React, { useEffect, useMemo } from "react";

import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";

import { useWindowSize } from "usehooks-ts";

import classnames from "classnames";

import styles from "./index.module.scss";

function ScoreBoardContent() {
  const { width = 0, height = 0 } = useWindowSize();

  // useEffect(() => {
  //   console.log("show wwidht, height", { width, height });
  // }, [width, height]);

  const widthForCard = useMemo(() => {
    const tempWidth = width / 2 - 100;
    return tempWidth > 174 ? tempWidth : 174;
  }, [width]);

  return (
    <div className={styles.container_score_board}>
      <div className={styles.title_score_board}>
        <p>Bảng xếp hạng</p>
      </div>

      <div
        className={styles.all_card_item}
        style={{ maxWidth: width, overflow: "auto" }}
      >
        <div className={styles.card_score_board}>
          <Card sx={{ width: widthForCard }}>
            <div className={styles.card_title_content}>
              <CardMedia
                component="img"
                height="36"
                image="/images/score_board/finance_background.jpg"
              ></CardMedia>
              <p>Tài chính</p>
            </div>

            <CardContent sx={{ padding: 0 }}>
              <div
                className={classnames(
                  "d-flex align-items-center justify-content-between",
                  styles.card_content_list
                )}
              >
                <span className={styles.first_position}>1</span>
                <div className="mx-1">
                  <img
                    src="/images/score_board/timo_digital_bank_icon.png"
                    alt="Timo Digital Bank"
                  />
                </div>
                <div
                  className={classnames(
                    "d-flex flex-column",
                    styles.content_text
                  )}
                >
                  <span>Timo Digital Bank</span>
                  <span>Timo Digital Bank là app...</span>
                </div>
              </div>
              <div
                className={classnames(
                  "d-flex align-items-center justify-content-between",
                  styles.card_content_list
                )}
              >
                <span className={styles.second_position}>2</span>

                <div className="mx-1">
                  <img
                    src="/images/score_board/finhay_icon.png"
                    alt="Timo Digital Bank"
                  />
                </div>
                <div
                  className={classnames(
                    "d-flex flex-column",
                    styles.content_text
                  )}
                >
                  <span>Finhay</span>
                  <span>Finhay là sản phẩm công...</span>
                </div>
              </div>
              <div
                className={classnames(
                  "d-flex align-items-center justify-content-between",
                  styles.card_content_list
                )}
              >
                <span className={styles.third_position}>3</span>
                <div>
                  <img
                    src="/images/score_board/mitrade_icon.png"
                    alt="Timo Digital Bank"
                  />
                </div>
                <div
                  className={classnames(
                    "d-flex flex-column",
                    styles.content_text
                  )}
                >
                  <span>Mitrade</span>
                  <span>Mitrade là một app đầu t...</span>
                </div>
              </div>
            </CardContent>
          </Card>
        </div>
        <div className={styles.card_score_board}>
          <Card sx={{ width: widthForCard }}>
            <div className={styles.card_title_content}>
              <CardMedia
                component="img"
                height="36"
                image="/images/score_board/insurance_background.jpg"
              />
              <p>Bảo hiểm</p>
            </div>

            <CardContent sx={{ padding: 0 }}>
              <div
                className={classnames(
                  "d-flex align-items-center justify-content-between",
                  styles.card_content_list
                )}
              >
                <span className={styles.first_position}>1</span>
                <div className="mx-1">
                  <img src="/images/score_board/ebh_icon.png" alt="eBH" />
                </div>
                <div
                  className={classnames(
                    "d-flex flex-column",
                    styles.content_text
                  )}
                >
                  <span>eBH</span>
                  <span>eBH là phần mềm B...</span>
                </div>
              </div>
              <div
                className={classnames(
                  "d-flex align-items-center justify-content-between",
                  styles.card_content_list
                )}
              >
                <span className={styles.second_position}>2</span>
                <div className="mx-1">
                  <img
                    src="/images/score_board/bhxh_vnpt_icon.png"
                    alt="BHXH VNPT"
                  />
                </div>
                <div
                  className={classnames(
                    "d-flex flex-column",
                    styles.content_text
                  )}
                >
                  <span>BHXH VNPT</span>
                  <span>VNPT luôn là đơn vị...</span>
                </div>
              </div>
              <div
                className={classnames(
                  "d-flex align-items-center justify-content-between",
                  styles.card_content_list
                )}
              >
                <span className={styles.third_position}>3</span>
                <div>
                  <img
                    src="/images/score_board/bhxh_efy_icon.png"
                    alt="BHXH EFY"
                  />
                </div>
                <div
                  className={classnames(
                    "d-flex flex-column",
                    styles.content_text
                  )}
                >
                  <span>BHXH EFY</span>
                  <span>đây là một trong nhữ...</span>
                </div>
              </div>
            </CardContent>
          </Card>
        </div>
        <div
          className={styles.card_score_board}
          // style={{ paddingRight: "12px" }}
        >
          <Card sx={{ width: widthForCard }}>
            <div className={styles.card_title_content}>
              <CardMedia
                component="img"
                height="36"
                image="/images/score_board/finance_background.jpg"
              ></CardMedia>
              <p>Tài chính</p>
            </div>

            <CardContent sx={{ padding: 0 }}>
              <div
                className={classnames(
                  "d-flex align-items-center justify-content-between",
                  styles.card_content_list
                )}
              >
                <span className={styles.first_position}>1</span>
                <div className="mx-1">
                  <img
                    src="/images/score_board/timo_digital_bank_icon.png"
                    alt="Timo Digital Bank"
                  />
                </div>
                <div
                  className={classnames(
                    "d-flex flex-column",
                    styles.content_text
                  )}
                >
                  <span>Timo Digital Bank</span>
                  <span>Timo Digital Bank là app...</span>
                </div>
              </div>
              <div
                className={classnames(
                  "d-flex align-items-center justify-content-between",
                  styles.card_content_list
                )}
              >
                <span className={styles.second_position}>2</span>
                <div className="mx-1">
                  <img
                    src="/images/score_board/finhay_icon.png"
                    alt="Timo Digital Bank"
                  />
                </div>
                <div
                  className={classnames(
                    "d-flex flex-column",
                    styles.content_text
                  )}
                >
                  <span>Finhay</span>
                  <span>Finhay là sản phẩm công...</span>
                </div>
              </div>
              <div
                className={classnames(
                  "d-flex align-items-center justify-content-between",
                  styles.card_content_list
                )}
              >
                <span className={styles.third_position}>3</span>
                <div>
                  <img
                    src="/images/score_board/mitrade_icon.png"
                    alt="Timo Digital Bank"
                  />
                </div>
                <div
                  className={classnames(
                    "d-flex flex-column",
                    styles.content_text
                  )}
                >
                  <span>Mitrade</span>
                  <span>Mitrade là một app đầu t...</span>
                </div>
              </div>
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  );
}

export default ScoreBoardContent;
