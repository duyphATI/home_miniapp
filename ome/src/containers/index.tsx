import React from "react";

import CatalogueContent from "./Catalogue";
import EntertainmentGamesContent from "./Entertainment_Games";
import FinancialAndInsuranceContent from "./Financial_n_Insurance";
import LovelyAppContent from "./Lovely_App";
import PromotionAndNeoServiceContent from "./Promotion_n_NeoService";
import RecentlyUsedContent from "./Recently_Used";
import ScoreBoardContent from "./Score_Board";
import TelecomDataCardGameContent from "./Telecom_Data_CardGame";
import DeliverAndBookingContent from "./Deliver_n_Booking";
import PurchaseContent from "./Purchase";
import HealthContent from "./Health";
import UtilityContent from "./Utility";
import SuggestOfficialAccountContent from "./Suggest_Official_Account";
import FirstSlickContent from "./First_Slick";
import PromotionDiscountContent from "./Promotion_Discount";
import HotDealContent from "./Hot_Deal";

function ContainerContent() {
  return (
    <React.Fragment>
      <FirstSlickContent></FirstSlickContent>

      <LovelyAppContent></LovelyAppContent>

      <ScoreBoardContent></ScoreBoardContent>

      <RecentlyUsedContent></RecentlyUsedContent>

      <HotDealContent></HotDealContent>

      <FinancialAndInsuranceContent></FinancialAndInsuranceContent>

      <CatalogueContent></CatalogueContent>

      <PromotionAndNeoServiceContent></PromotionAndNeoServiceContent>

      <EntertainmentGamesContent></EntertainmentGamesContent>

      <PromotionDiscountContent></PromotionDiscountContent>

      <TelecomDataCardGameContent></TelecomDataCardGameContent>

      <DeliverAndBookingContent></DeliverAndBookingContent>

      <PurchaseContent></PurchaseContent>

      <HealthContent></HealthContent>

      <UtilityContent></UtilityContent>

      <SuggestOfficialAccountContent></SuggestOfficialAccountContent>
    </React.Fragment>
  );
}

export default ContainerContent;
