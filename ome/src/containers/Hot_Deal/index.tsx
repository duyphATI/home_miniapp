import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import CardContent from "@mui/material/CardContent";

function HotDealContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListBanner = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_hot_deal}>
      <div className={styles.title_hot_deal}>
        <p>Hot deal</p>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_hot_deal}
        style={{
          maxWidth: widthForListBanner,
          overflow: "auto",
        }}
      >
        <div className="d-flex align-items-center flex-column">
          <div className={classnames(styles.card_hot_deal)}>
            <Card style={{ padding: "8px 8px 12px 8px" }}>
              <CardMedia
                sx={{ width: "301px", height: "145px" }}
                component="img"
                image="/images/hot_deal/banner_hot_deal_1.png"
              />
              <p>Thanh toán hóa đơn hoàn tiền 40K</p>
            </Card>
          </div>
        </div>
        <div className="d-flex align-items-center flex-column ">
          <div className={styles.card_hot_deal}>
            <Card style={{ padding: "8px 8px 12px 8px" }}>
              <CardMedia
                sx={{ width: "301px", height: "145pxF" }}
                component="img"
                image="/images/hot_deal/banner_hot_deal_2.png"
              />{" "}
              <p>Thanh toán hóa đơn hoàn tiền 40K</p>
            </Card>
          </div>
        </div>
        <div className="d-flex align-items-center flex-column ">
          <div className={styles.card_hot_deal}>
            <Card style={{ padding: "8px 8px 12px 8px" }}>
              <CardMedia
                sx={{ width: "301px", height: "145px" }}
                component="img"
                image="/images/hot_deal/banner_hot_deal_1.png"
              />
              <p>Thanh toán hóa đơn hoàn tiền 40K</p>
            </Card>
          </div>
        </div>
        <div className="d-flex align-items-center flex-column ">
          <div className={styles.card_hot_deal}>
            <Card style={{ padding: "8px 8px 12px 8px" }}>
              <CardMedia
                sx={{ width: "301px", height: "145px" }}
                component="img"
                image="/images/hot_deal/banner_hot_deal_2.png"
              />{" "}
              <p>Thanh toán hóa đơn hoàn tiền 40K</p>
            </Card>
          </div>
        </div>
        <div className="d-flex align-items-center flex-column ">
          <div className={styles.card_hot_deal}>
            <Card style={{ padding: "8px 8px 12px 8px" }}>
              <CardMedia
                sx={{ width: "301px", height: "145px" }}
                component="img"
                image="/images/hot_deal/banner_hot_deal_1.png"
              />
              <p>Thanh toán hóa đơn hoàn tiền 40K</p>
            </Card>
          </div>
        </div>
      </div>
    </div>
  );
}

export default HotDealContent;
