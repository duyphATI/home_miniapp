import { useMemo } from "react";

import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";

import { useWindowSize } from "usehooks-ts";

import classnames from "classnames";

import styles from "./index.module.scss";

function SuggestOfficalAccountContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForCard = useMemo(() => {
    const tempWidth = width / 2 - 100;
    return tempWidth > 213 ? tempWidth : 213;
  }, [width]);

  return (
    <div className={styles.container_suggest_offical_account}>
      <div className={styles.title_suggest_offical_account}>
        <p>Gợi ý Offical Account</p>
      </div>

      <div
        className={styles.all_card_item}
        style={{ maxWidth: width, overflow: "auto" }}
      >
        <div className={styles.card_suggest_offical_account}>
          <Card sx={{ width: widthForCard }}>
            <div className={styles.card_title_content}>
              <CardMedia
                component="img"
                height="80px"
                image="/images/suggest_offical_account/hoaphat_background.png"
              ></CardMedia>

              <img
                src="/images/suggest_offical_account/hoaphat_logo.png"
                alt="Hòa Phát"
              />
            </div>

            <CardContent sx={{ padding: 0 }}>
              <div
                className={classnames(
                  "d-flex align-items-center justify-content-between flex-column",
                  styles.card_content_list
                )}
              >
                <div className={styles.item_card_content}>
                  <p>Tập đoàn Hòa Phát</p>
                  <p>Hà Nội</p>
                </div>

                <div className={styles.item_card_button}>
                  <Button
                    variant="contained"
                    sx={{
                      width: "100%",
                      textTransform: "unset",
                      marginBottom: "12px",
                      height: "28px",
                    }}
                  >
                    <span>Quan tâm</span>
                  </Button>
                  <Button
                    variant="outlined"
                    sx={{
                      width: "100%",
                      textTransform: "unset",
                      height: "28px",
                    }}
                  >
                    <span>Xem chi tiết</span>
                  </Button>
                </div>
              </div>
            </CardContent>
          </Card>
        </div>

        <div className={styles.card_suggest_offical_account}>
          <Card sx={{ width: widthForCard }}>
            <div className={styles.card_title_content}>
              <CardMedia
                component="img"
                height="80"
                image="/images/suggest_offical_account/vingroup_background.png"
              />

              <img
                src="/images/suggest_offical_account/vingroup_logo.png"
                alt="Vingroup"
              />
            </div>

            <CardContent sx={{ padding: 0 }}>
              <div
                className={classnames(
                  "d-flex align-items-center justify-content-between flex-column",
                  styles.card_content_list
                )}
              >
                <div className={styles.item_card_content}>
                  <p>Tập đoàn Vingroup</p>
                  <p>Hà Nội</p>
                </div>

                <div className={styles.item_card_button}>
                  <Button
                    variant="contained"
                    sx={{
                      width: "100%",
                      textTransform: "unset",
                      marginBottom: "12px",
                      height: "28px",
                    }}
                  >
                    <span>Quan tâm</span>
                  </Button>
                  <Button
                    variant="outlined"
                    sx={{
                      width: "100%",
                      textTransform: "unset",
                      height: "28px",
                    }}
                  >
                    <span>Xem chi tiết</span>
                  </Button>
                </div>
              </div>
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  );
}

export default SuggestOfficalAccountContent;
