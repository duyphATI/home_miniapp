import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import MoreIcon from "../../icons/more_icon";

function PurchaseContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_purchase}>
      <div className={styles.title_purchase}>
        <p>Mua sắm</p>{" "}
        <div className={styles.suggest_text}>
          <div>Gợi ý</div>
        </div>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_purchase}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        <div className="d-flex align-items-start justify-content-start  mb-3 ">
          <div className={classnames(styles.card_purchase)}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/purchase/shopee_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>Shopee</p>
            <p>
              Shopee là sàn giao dịch thương mại điện tử có trụ sở đặt tại
              Singapore, thuộc sở hữu của Sea Ltd, được thành lập vào năm 2009
              bởi Lý Tiểu Đông.
            </p>
            <p>Mua sắm | + 100k người dùng</p>
          </div>
        </div>
        <div className="d-flex align-items-start justify-content-start  mb-3  ">
          <div className={styles.card_purchase}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/purchase/tiki_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>Tiki</p>
            <p>
              Tiki là viết tắt của “Tìm kiếm & Tiết kiệm”, là tên của website
              thương mại điện tử Việt Nam. Thành lập từ tháng 3 năm 2010, Tiki
              hiện đang là trang thương mại điện tử lọt top 2 tại Việt Nam và
              top 6 tại khu vực Đông Nam Á.
            </p>
            <p>Mua sắm | + 100k người dùng</p>
          </div>
        </div>
        <div className="d-flex align-items-start justify-content-start  mb-3  ">
          <div className={styles.card_purchase}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/purchase/lazada_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>Lazada</p>
            <p>
              Lazada Việt Nam là một sàn giao dịch thương mại điện tử, là một
              phần của Lazada Group – tập đoàn thương mại điện tử đa quốc gia và
              hiện đang có chi nhánh tại Indonesia, Philippines, Singapore, Thái
              Lan và Malaysia. Tập đoàn Lazada hiện thuộc sở hữu của tập đoàn
              Alibaba.
            </p>
            <p>Mua sắm | + 100k người dùng</p>
          </div>
        </div>
      </div>

      <div className={styles.more_text}>
        <div className={styles.icon_button}>
          <MoreIcon></MoreIcon>
          <span>Xem thêm</span>
        </div>
      </div>
    </div>
  );
}

export default PurchaseContent;
