import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import MoreIcon from "../../icons/more_icon";

function FinancialAndInsuranceContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_financial_n_insurance}>
      <div className={styles.title_financial_n_insurance}>
        <p>Tài chính và bảo hiểm</p>
        <div className={styles.suggest_text}>
          <div>Gợi ý</div>
        </div>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_financial_n_insurance}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        <div className="d-flex align-items-start justify-content-start  mb-3 ">
          <div className={classnames(styles.card_financial_n_insurance)}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/financial_n_insurance/finhay_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>Finhay</p>
            <p>
              Công ty Cổ phần Finhay Việt Nam (Finhay) là một công ty Fintech
              cung cấp nền tảng công nghệ được công nhận là doanh nghiệp khoa
              học công nghệ vận hành theo.
            </p>
            <p>Tài chính | + 100k người dùng</p>
          </div>
        </div>
        <div className="d-flex align-items-start justify-content-start  mb-3  ">
          <div className={styles.card_financial_n_insurance}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/financial_n_insurance/mitrade_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>Mitrade</p>
            <p>
              Mitrade là một nhà môi giới giao dịch CFD toàn cầu được thành lập
              vào năm 2018 với sứ mệnh giúp các nhà đầu tư giao dịch đơn giản
              hơn. Trụ sở chính của Mitrade đặt tại Melbourne, Úc và hiện phục
              vụ khách hàng đến từ khắp mọi nơi thế giới.
            </p>
            <p>Tài chính | + 100k người dùng</p>
          </div>
        </div>
        <div className="d-flex align-items-start justify-content-start  mb-3  ">
          <div className={styles.card_financial_n_insurance}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/financial_n_insurance/timo_digital_bank_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>Timo Digital Bank</p>
            <p>
              Ứng dụng Timo không chỉ là một công cụ eBanking thông thường, mà
              thực sự là một ngân hàng có khả năng thực hiện tất cả các nhiệm vụ
              của một ngân hàng truyền thống như:
            </p>
            <p>Tài chính | + 100k người dùng</p>
          </div>
        </div>
      </div>

      <div className={styles.more_text}>
        <div className={styles.icon_button}>
          <MoreIcon></MoreIcon>
          <span>Xem thêm</span>
        </div>
      </div>
    </div>
  );
}

export default FinancialAndInsuranceContent;
