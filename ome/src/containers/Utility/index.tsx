import Box from "@mui/material/Box";
import React, { useMemo } from "react";

import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

import classnames from "classnames";
import styles from "./index.module.scss";
import { useWindowSize } from "usehooks-ts";
import MoreIcon from "../../icons/more_icon";

function UtilityContent() {
  const { width = 0, height = 0 } = useWindowSize();

  const widthForListItem = useMemo(() => {
    const tempWidth = width - 40;
    return tempWidth;
  }, [width]);

  return (
    <div className={styles.container_utility}>
      <div className={styles.title_utility}>
        <p>Tiện ích</p>{" "}
        <div className={styles.suggest_text}>
          <div>Gợi ý</div>
        </div>
        <p>Xem tất cả</p>
      </div>

      <div
        className={styles.box_utility}
        style={{
          maxWidth: widthForListItem,
          overflow: "auto",
        }}
      >
        <div className="d-flex align-items-start justify-content-start  mb-3 ">
          <div className={classnames(styles.card_utility)}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/utility/chat_gpt_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>Chat GPT</p>
            <p>
              ChatGPT, viết tắt của Chat Generative Pre-training Transformer, là
              một chatbot do công ty OpenAI của Mỹ phát triển và ra mắt vào
              tháng 11 năm 2022. ChatGPT được xây dựng dựa trên GPT-3.5 - một
              dòng mô hình ngôn ngữ lớn của OpenAI đồng thời được tinh chỉnh
              bằng cả hai kỹ thuật học tăng cường lẫn học có giám sát. 
            </p>
            <p>Tiện ích | + 100k người dùng</p>
          </div>
        </div>
        <div className="d-flex align-items-start justify-content-start  mb-3  ">
          <div className={styles.card_utility}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/utility/thien_nguyen_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>Thiện Nguyện</p>
            <p>
              App Thiện nguyện là nền tảng kết nối giữa cá nhân, tổ chức gây quỹ
              với cộng đồng, lan tỏa tinh thần thiện nguyện đến với mọi
              người. App Thiện nguyện ra đời ...
            </p>
            <p>Tiện ích | + 100k người dùng</p>
          </div>
        </div>
        <div className="d-flex align-items-start justify-content-start  mb-3  ">
          <div className={styles.card_utility}>
            <Card
              style={{ width: "48px", height: "48px", borderRadius: "4px" }}
            >
              <CardMedia
                component="img"
                width="48"
                height="48"
                image="/images/utility/bds_homedy_icon.png"
              />
            </Card>
          </div>
          <div className={classnames(styles.content_item_list, "px-2")}>
            <p>Bất động sản Homedy</p>
            <p>
              Homedy App là ứng dụng tiện lợi giúp bạn tìm kiếm miễn phí, nhanh
              chóng và chính xác mọi thông tin về bất động sản, cho thuê - mua
              bán nhà đất, chung cư, nhà riêng, phòng trọ, biệt thự liền kề,...
              tại TP Hồ Chí Minh, Hà Nội, Đà Nẵng, Đồng Nai, Bình Dương, Long
              An, Bà Rịa Vũng Tàu... Với Homedy App, người dùng có thể:
            </p>
            <p>Tiện ích | + 100k người dùng</p>
          </div>
        </div>
      </div>

      <div className={styles.more_text}>
        <div className={styles.icon_button}>
          <MoreIcon></MoreIcon>
          <span>Xem thêm</span>
        </div>
      </div>
    </div>
  );
}

export default UtilityContent;
